#include <Bahamut.h>

#include <forge/system/data/DataManager.h>
#include <forge/system/window/WindowManager.h>
#include <forge/2d/camera/CameraManager.h>

#include <data/animations/AnimationCatalog.h>
#include <data/entities/EntityCatalog.h>
#include <data/sprites/SpriteCatalog.h>
#include <data/textures/TextureCatalog.h>
#include <data/audio/MusicCatalog.h>
#include <data/audio/SoundBufferCatalog.h>
#include <data/audio/SoundCatalog.h>
#include <data/fonts/FontCatalog.h>
#include <data/fxs/FXCatalog.h>
#include <data/DataNameIds.h>


//----------------------------------------------------------------------------
World*  g_World = nullptr;

//----------------------------------------------------------------------------
Bahamut::Bahamut()
    : m_FreeRoamingState(*this)
{
}

//----------------------------------------------------------------------------
Bahamut::~Bahamut()
{
}

//----------------------------------------------------------------------------
void Bahamut::Init(const std::string& _gameName)
{
    AbstractForgeGame::Init(_gameName);

    DataManager::GetInstance().RegisterCatalog<AnimationCatalog>();
    DataManager::GetInstance().RegisterCatalog<EntityCatalog>();
    DataManager::GetInstance().RegisterCatalog<SpriteCatalog>();
    DataManager::GetInstance().RegisterCatalog<TextureCatalog>();
    DataManager::GetInstance().RegisterCatalog<MusicCatalog>();
    DataManager::GetInstance().RegisterCatalog<SoundBufferCatalog>();
    DataManager::GetInstance().RegisterCatalog<SoundCatalog>();
    DataManager::GetInstance().RegisterCatalog<FontCatalog>();
    DataManager::GetInstance().RegisterCatalog<FXCatalog>();

    const sf::Window* window = WindowManager::GetInstance().GetWindow();

    CameraManager::GetInstance().CreateCamera(CameraType::TopDown);
    CameraManager::GetInstance().GetMainCamera().Resize(static_cast<float>(window->getSize().x),
                                                        static_cast<float>(window->getSize().y));
    CameraManager::GetInstance().GetMainCamera().SetLookAt(1000.0f, 1000.0f);

    InitWorld();

    PushState(m_FreeRoamingState);
}

//----------------------------------------------------------------------------
void Bahamut::Quit()
{
    PopState();

    m_World.Destroy();

    AbstractForgeGame::Quit();
}

//----------------------------------------------------------------------------
void Bahamut::InitWorld()
{
    m_World.Init(2000, 2000);

    const EntityCatalog* catalog = DataManager::GetInstance().GetCatalog<EntityCatalog>();

    if (catalog != nullptr)
    {
        Entity::Ptr mainPC = catalog->Get(DataNameIds::Entities::k_PlayableChar);
        mainPC->As<Entity>()->SetPosition(1000.0f, 1000.0f);

        m_World.AddEntity(mainPC);


        Entity::Ptr referee = catalog->Get(DataNameIds::Entities::k_GameReferee);
        referee->As<Entity>()->SetPosition(1000.0f, 1000.0f);

        m_World.AddEntity(referee);
    }

    g_World = &m_World;
}

