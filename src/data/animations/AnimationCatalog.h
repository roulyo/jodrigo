#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/2d/drawable/Animation.h>

#include <data/animations/AnimationFactories.h>

//----------------------------------------------------------------------------
class AnimationCatalog : public DataCatalog<Animation>
{
    forge_DeclCatalog(AnimationCatalog);

public:
    AnimationCatalog();
    ~AnimationCatalog();

private:
    IdleLAnimationFactory   m_IdleLAnimationFactory;
    IdleRAnimationFactory	m_IdleRAnimationFactory;

    PunchNAnimationFactory	m_PunchNAnimationFactory;
    PunchSAnimationFactory	m_PunchSAnimationFactory;
    PunchEAnimationFactory	m_PunchEAnimationFactory;
    PunchWAnimationFactory	m_PunchWAnimationFactory;

};
