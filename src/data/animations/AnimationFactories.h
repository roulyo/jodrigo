#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/2d/drawable/Animation.h>

//----------------------------------------------------------------------------
class IdleLAnimationFactory : public AbstractDataFactory
{
public:
    Animation::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class IdleRAnimationFactory : public AbstractDataFactory
{
public:
    Animation::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class PunchNAnimationFactory : public AbstractDataFactory
{
public:
    Animation::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class PunchSAnimationFactory : public AbstractDataFactory
{
public:
    Animation::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class PunchEAnimationFactory : public AbstractDataFactory
{
public:
    Animation::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class PunchWAnimationFactory : public AbstractDataFactory
{
public:
    Animation::Ptr Create() const override;

};
