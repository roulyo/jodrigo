#include <data/animations/AnimationFactories.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

static const unsigned k_DefaultAnimDuration = 500;

//----------------------------------------------------------------------------
Animation::Ptr IdleLAnimationFactory::Create() const
{
    Animation* anim = new Animation(0, 0, 2, 500);
    anim->SetDataNameId(Animations::k_IdleL);

    return Animation::Ptr(anim);
}

//----------------------------------------------------------------------------
Animation::Ptr IdleRAnimationFactory::Create() const
{
    Animation* anim = new Animation(0, 1, 2, 500);
    anim->SetDataNameId(Animations::k_IdleR);

    return Animation::Ptr(anim);
}

//----------------------------------------------------------------------------
Animation::Ptr PunchNAnimationFactory::Create() const
{
    Animation* anim = new Animation(0, 1, 4, 330);
    anim->SetDataNameId(Animations::k_PunchN);
    anim->SetIsRepeatable(false);

    return Animation::Ptr(anim);
}

//----------------------------------------------------------------------------
Animation::Ptr PunchSAnimationFactory::Create() const
{
    Animation* anim = new Animation(0, 3, 4, 330);
    anim->SetDataNameId(Animations::k_PunchS);
    anim->SetIsRepeatable(false);

    return Animation::Ptr(anim);
}

//----------------------------------------------------------------------------
Animation::Ptr PunchEAnimationFactory::Create() const
{
    Animation* anim = new Animation(0, 2, 4, 330);
    anim->SetDataNameId(Animations::k_PunchE);
    anim->SetIsRepeatable(false);

    return Animation::Ptr(anim);
}

//----------------------------------------------------------------------------
Animation::Ptr PunchWAnimationFactory::Create() const
{
    Animation* anim = new Animation(0, 0, 4, 330);
    anim->SetDataNameId(Animations::k_PunchW);
    anim->SetIsRepeatable(false);

    return Animation::Ptr(anim);
}
