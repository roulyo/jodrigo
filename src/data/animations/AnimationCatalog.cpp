#include <data/animations/AnimationCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
AnimationCatalog::AnimationCatalog()
{
    RegisterData(Animations::k_IdleL,   m_IdleLAnimationFactory);
    RegisterData(Animations::k_IdleR,	m_IdleRAnimationFactory);

    RegisterData(Animations::k_PunchN, m_PunchNAnimationFactory);
    RegisterData(Animations::k_PunchS, m_PunchSAnimationFactory);
    RegisterData(Animations::k_PunchE, m_PunchEAnimationFactory);
    RegisterData(Animations::k_PunchW, m_PunchWAnimationFactory);
}

//----------------------------------------------------------------------------
AnimationCatalog::~AnimationCatalog()
{
    m_Factories.clear();
}
