#pragma once

#include <forge/system/data/Data.h>

namespace DataNameIds
{
    // Textures
    namespace Textures
    {
        extern const DataNameId k_CharSpriteSheet;
        extern const DataNameId k_CovidSpriteSheet;
        extern const DataNameId k_PunchSpriteSheet;
    }

    // Animations
    namespace Animations
    {
        extern const DataNameId k_IdleL;
        extern const DataNameId k_IdleR;

        extern const DataNameId k_PunchN;
        extern const DataNameId k_PunchS;
        extern const DataNameId k_PunchE;
        extern const DataNameId k_PunchW;
    }

    // Sprites
    namespace Sprites
    {
        extern const DataNameId k_Jodrigo;
        extern const DataNameId k_Covid;
        extern const DataNameId k_Punch;
    }

    // Entities
    namespace Entities
    {
        extern const DataNameId k_PlayableChar;
        extern const DataNameId k_PunchAttack;
        extern const DataNameId k_EnemyChar;
        extern const DataNameId k_GameReferee;
    }

    // SoundBuffers
    namespace SoundBuffers
    {
        extern const DataNameId k_HitGive;
        extern const DataNameId k_HitTake;
        extern const DataNameId k_EnemyDeath;
    }

    // Sounds
    namespace Sounds
    {
        extern const DataNameId k_HitGive;
        extern const DataNameId k_HitTake;
        extern const DataNameId k_EnemyDeath;
    }

    // Musics
    namespace Musics
    {
        extern const DataNameId k_MainTheme;
    }

    // Fonts
    namespace Fonts
    {
        extern const DataNameId k_MainFont;
    }

    // FXs
    namespace FXs
    {
    }
}
