#include <data/DataNameIds.h>

namespace DataNameIds
{
    // Textures
    namespace Textures
    {
        const DataNameId k_CharSpriteSheet  = DataNameId("Textures::k_CharSpriteSheet");
        const DataNameId k_CovidSpriteSheet = DataNameId("Textures::k_CovidSpriteSheet");
        const DataNameId k_PunchSpriteSheet = DataNameId("Textures::k_PunchSpriteSheet");
    }

    // Animations
    namespace Animations
    {
        const DataNameId k_IdleL            = DataNameId("Textures::k_IdleL");
        const DataNameId k_IdleR			= DataNameId("Textures::k_IdleR");

        const DataNameId k_PunchN           = DataNameId("Textures::k_PunchN");
        const DataNameId k_PunchS           = DataNameId("Textures::k_PunchS");
        const DataNameId k_PunchE           = DataNameId("Textures::k_PunchE");
        const DataNameId k_PunchW           = DataNameId("Textures::k_PunchW");
    }

    // Sprites
    namespace Sprites
    {
        const DataNameId k_Jodrigo	        = DataNameId("Sprites::k_Jodrigo");
        const DataNameId k_Covid            = DataNameId("Sprites::k_Covid");
        const DataNameId k_Punch            = DataNameId("Sprites::k_Punch");
    }

    // Entities
    namespace Entities
    {
        const DataNameId k_PlayableChar     = DataNameId("Entities::k_PlayableChar");
        const DataNameId k_PunchAttack      = DataNameId("Entities::k_PunchAttack");
        const DataNameId k_EnemyChar        = DataNameId("Entities::k_EnemyChar");
        const DataNameId k_GameReferee      = DataNameId("Entities::k_GameReferee");
    }
    
    // SoundBuffers
    namespace SoundBuffers
    {
        const DataNameId k_HitGive          = DataNameId("SoundBuffers::k_HitGive");
        const DataNameId k_HitTake          = DataNameId("SoundBuffers::k_HitTake");
        const DataNameId k_EnemyDeath       = DataNameId("SoundBuffers::k_EnemyDeath");
    }

    // Sounds
    namespace Sounds
    {
        const DataNameId k_HitGive          = DataNameId("Sounds::k_HitGive");
        const DataNameId k_HitTake          = DataNameId("Sounds::k_HitTake");
        const DataNameId k_EnemyDeath       = DataNameId("Sounds::k_EnemyDeath");
    }

    // Musics
    namespace Musics
    {
        const DataNameId k_MainTheme        = DataNameId("Musics::k_MainTheme");
    }

    // Fonts
    namespace Fonts
    {
        const DataNameId k_MainFont         = DataNameId("Fonts::k_MainFont");

    }

    // FXs
    namespace FXs
    {
    }
}
