#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/2d/drawable/Font.h>

//----------------------------------------------------------------------------
class MainFontFontFactory : public AbstractDataFactory
{
public:
    Font::Ptr Create() const override;

private:
    static Font::Ptr m_Font;

};
