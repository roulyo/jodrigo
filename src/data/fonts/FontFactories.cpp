#include <data/fonts/FontFactories.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Font::Ptr MainFontFontFactory::m_Font = nullptr;

//----------------------------------------------------------------------------
Font::Ptr MainFontFontFactory::Create() const
{
    if (m_Font == nullptr)
    {
        Font* font = new Font("assets/BrokenChalk.ttf");
        font->SetDataNameId(Fonts::k_MainFont);

        m_Font.reset(font);
    }

    return m_Font;
}
