#include <data/fonts/FontCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
FontCatalog::FontCatalog()
{
    RegisterData(Fonts::k_MainFont, m_MainFontFontFactory);
}

//----------------------------------------------------------------------------
FontCatalog::~FontCatalog()
{
    m_Factories.clear();
}
