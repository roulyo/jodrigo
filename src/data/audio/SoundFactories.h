#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/audio/Sound.h>

//----------------------------------------------------------------------------
class HitGiveSoundFactory : public AbstractDataFactory
{
public:
    Sound::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class HitTakeSoundFactory : public AbstractDataFactory
{
public:
    Sound::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class EnemyDeathSoundFactory : public AbstractDataFactory
{
public:
    Sound::Ptr Create() const override;

};
