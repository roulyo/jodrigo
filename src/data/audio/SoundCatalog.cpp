#include <data/audio/SoundCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
SoundCatalog::SoundCatalog()
{
    RegisterData(Sounds::k_HitGive, m_HitGiveSoundFactory);
    RegisterData(Sounds::k_HitTake, m_HitTakeSoundFactory);
    RegisterData(Sounds::k_EnemyDeath, m_EnemyDeathSoundFactory);
}

//----------------------------------------------------------------------------
SoundCatalog::~SoundCatalog()
{
    m_Factories.clear();
}
