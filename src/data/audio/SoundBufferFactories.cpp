#include <data/audio/SoundBufferFactories.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
SoundBuffer::Ptr HitGiveSoundBufferFactory::m_SoundBuffer = nullptr;

//----------------------------------------------------------------------------
SoundBuffer::Ptr HitGiveSoundBufferFactory::Create() const
{
    if (m_SoundBuffer == nullptr)
    {
        SoundBuffer* soundBuffer = new SoundBuffer("assets/ouwawa.wav");
        soundBuffer->SetDataNameId(SoundBuffers::k_HitGive);

        m_SoundBuffer.reset(soundBuffer);
    }

    return m_SoundBuffer;
}

//----------------------------------------------------------------------------
SoundBuffer::Ptr HitTakeSoundBufferFactory::m_SoundBuffer = nullptr;

//----------------------------------------------------------------------------
SoundBuffer::Ptr HitTakeSoundBufferFactory::Create() const
{
    if (m_SoundBuffer == nullptr)
    {
        SoundBuffer* soundBuffer = new SoundBuffer("assets/argh.wav");
        soundBuffer->SetDataNameId(SoundBuffers::k_HitTake);

        m_SoundBuffer.reset(soundBuffer);
    }

    return m_SoundBuffer;
}

//----------------------------------------------------------------------------
SoundBuffer::Ptr EnemyDeathSoundBufferFactory::m_SoundBuffer = nullptr;

//----------------------------------------------------------------------------
SoundBuffer::Ptr EnemyDeathSoundBufferFactory::Create() const
{
    if (m_SoundBuffer == nullptr)
    {
        SoundBuffer* soundBuffer = new SoundBuffer("assets/piou.wav");
        soundBuffer->SetDataNameId(SoundBuffers::k_EnemyDeath);

        m_SoundBuffer.reset(soundBuffer);
    }

    return m_SoundBuffer;
}
