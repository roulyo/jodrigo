#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/audio/SoundBuffer.h>

#include <data/audio/SoundBufferFactories.h>

//----------------------------------------------------------------------------
class SoundBufferCatalog : public DataCatalog<SoundBuffer>
{
    forge_DeclCatalog(SoundBufferCatalog);

public:
    SoundBufferCatalog();
    ~SoundBufferCatalog();

private:
    HitGiveSoundBufferFactory       m_HitGiveSoundBufferFactory;
    HitTakeSoundBufferFactory       m_HitTakeSoundBufferFactory;
    EnemyDeathSoundBufferFactory    m_EnemyDeathSoundBufferFactory;

};
