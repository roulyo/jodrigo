#include <data/audio/MusicCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
MusicCatalog::MusicCatalog()
{
    RegisterData(Musics::k_MainTheme, m_MainThemeMusicFactory);
}

//----------------------------------------------------------------------------
MusicCatalog::~MusicCatalog()
{
    m_Factories.clear();
}
