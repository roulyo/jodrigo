#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/audio/Sound.h>

#include <data/audio/SoundFactories.h>

//----------------------------------------------------------------------------
class SoundCatalog : public DataCatalog<Sound>
{
    forge_DeclCatalog(SoundCatalog);

public:
    SoundCatalog();
    ~SoundCatalog();

private:
    HitGiveSoundFactory     m_HitGiveSoundFactory;
    HitTakeSoundFactory     m_HitTakeSoundFactory;
    EnemyDeathSoundFactory  m_EnemyDeathSoundFactory;

};
