#include <data/audio/MusicFactories.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Music::Ptr MainThemeMusicFactory::m_Music = nullptr;

//----------------------------------------------------------------------------
Music::Ptr MainThemeMusicFactory::Create() const
{
    if (m_Music == nullptr)
    {
        Music* music = new Music("assets/jodrigo_theme.ogg");
        music->SetDataNameId(Musics::k_MainTheme);

        m_Music.reset(music);
    }

    return m_Music;
}
