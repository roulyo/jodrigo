#include <data/audio/SoundBufferCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
SoundBufferCatalog::SoundBufferCatalog()
{
    RegisterData(SoundBuffers::k_HitGive, m_HitGiveSoundBufferFactory);
    RegisterData(SoundBuffers::k_HitTake, m_HitTakeSoundBufferFactory);
    RegisterData(SoundBuffers::k_EnemyDeath, m_EnemyDeathSoundBufferFactory);
}

//----------------------------------------------------------------------------
SoundBufferCatalog::~SoundBufferCatalog()
{
    m_Factories.clear();
}
