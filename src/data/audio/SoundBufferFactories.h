#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/audio/SoundBuffer.h>

//----------------------------------------------------------------------------
class HitGiveSoundBufferFactory : public AbstractDataFactory
{
public:
    SoundBuffer::Ptr Create() const override;

private:
    static SoundBuffer::Ptr m_SoundBuffer;

};

//----------------------------------------------------------------------------
class HitTakeSoundBufferFactory : public AbstractDataFactory
{
public:
    SoundBuffer::Ptr Create() const override;

private:
    static SoundBuffer::Ptr m_SoundBuffer;

};

//----------------------------------------------------------------------------
class EnemyDeathSoundBufferFactory : public AbstractDataFactory
{
public:
    SoundBuffer::Ptr Create() const override;

private:
    static SoundBuffer::Ptr m_SoundBuffer;

};
