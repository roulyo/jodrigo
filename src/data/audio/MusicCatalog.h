#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/audio/Music.h>

#include <data/audio/MusicFactories.h>

//----------------------------------------------------------------------------
class MusicCatalog : public DataCatalog<Music>
{
    forge_DeclCatalog(MusicCatalog);

public:
    MusicCatalog();
    ~MusicCatalog();

private:
    MainThemeMusicFactory   m_MainThemeMusicFactory;

};
