#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/audio/Music.h>

//----------------------------------------------------------------------------
class MainThemeMusicFactory : public AbstractDataFactory
{
public:
    Music::Ptr Create() const override;

private:
    static Music::Ptr m_Music;

};
