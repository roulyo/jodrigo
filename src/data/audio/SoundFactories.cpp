#include <data/audio/SoundFactories.h>

#include <data/audio/SoundBufferCatalog.h>

#include <data/DataNameIds.h>

#include <forge/system/data/DataManager.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Sound::Ptr HitGiveSoundFactory::Create() const
{
    const SoundBufferCatalog* catalog = DataManager::GetInstance().GetCatalog<SoundBufferCatalog>();

    Sound* sound = nullptr;

    if (catalog != nullptr)
    {
        sound = new Sound(catalog->Get(SoundBuffers::k_HitGive));
    }

    return Sound::Ptr(sound);
}

//----------------------------------------------------------------------------
Sound::Ptr HitTakeSoundFactory::Create() const
{
    const SoundBufferCatalog* catalog = DataManager::GetInstance().GetCatalog<SoundBufferCatalog>();

    Sound* sound = nullptr;

    if (catalog != nullptr)
    {
        sound = new Sound(catalog->Get(SoundBuffers::k_HitTake));
    }

    return Sound::Ptr(sound);
}

//----------------------------------------------------------------------------
Sound::Ptr EnemyDeathSoundFactory::Create() const
{
    const SoundBufferCatalog* catalog = DataManager::GetInstance().GetCatalog<SoundBufferCatalog>();

    Sound* sound = nullptr;

    if (catalog != nullptr)
    {
        sound = new Sound(catalog->Get(SoundBuffers::k_EnemyDeath));
    }

    return Sound::Ptr(sound);
}
