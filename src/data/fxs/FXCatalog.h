#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/core/fx/FX.h>

#include <data/fxs/FXFactories.h>

//----------------------------------------------------------------------------
class FXCatalog : public DataCatalog<FX>
{
    forge_DeclCatalog(FXCatalog);

public:
    FXCatalog();
    ~FXCatalog();

private:

};
