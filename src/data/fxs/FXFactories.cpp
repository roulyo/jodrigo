#include <data/fxs/FXFactories.h>

#include <data/sprites/SpriteCatalog.h>
#include <data/audio/SoundCatalog.h>
#include <data/DataNameIds.h>

#include <forge/system/data/DataManager.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
FX::Ptr NullFXFactory::Create() const
{
    const SpriteCatalog* spriteCatalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>();
    const SoundCatalog*  soundCatalog = DataManager::GetInstance().GetCatalog<SoundCatalog>();

    FX* fx = nullptr;

    if (spriteCatalog != nullptr && soundCatalog != nullptr)
    {
    }

    return FX::Ptr(fx);
}
