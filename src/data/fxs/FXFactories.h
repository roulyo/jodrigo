#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/core/fx/FX.h>

//----------------------------------------------------------------------------
class NullFXFactory : public AbstractDataFactory
{
public:
    FX::Ptr Create() const override;

};
