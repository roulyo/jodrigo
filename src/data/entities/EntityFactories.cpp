#include <data/entities/EntityFactories.h>

#include <forge/system/data/DataManager.h>

#include <data/sprites/SpriteCatalog.h>

#include <components/PlayableCharacterComponent.h>
#include <components/PhysicableComponent.h>
#include <components/RenderableComponent.h>
#include <components/EntityLinkComponent.h>
#include <components/EnemyCharacterComponent.h>
#include <components/GameRefereeComponent.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Entity::Ptr PlayableCharEntityFactory::Create() const
{
    const SpriteCatalog* catalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>();

    Entity* entity = new Entity();
    entity->SetDataNameId(Entities::k_PlayableChar);

    RenderableComponent& renderComp = entity->AddComponent<RenderableComponent>();
    if (catalog != nullptr)
    {
        renderComp.SetSprite(catalog->Get(Sprites::k_Jodrigo));
        renderComp.GetSprite().SetZValue(1.0f);

        renderComp.GetSprite().PlayAnimation(DataNameIds::Animations::k_IdleL);
    }

    entity->AddComponent<PhysicableComponent>();
    entity->AddComponent<PlayableCharacterComponent>();
    entity->AddComponent<EntityLinkComponent>();

    entity->SetSize(100.0f, 200.0f);

    return Entity::Ptr(entity);
}

//----------------------------------------------------------------------------
Entity::Ptr PunchAttackEntityFactory::Create() const
{
    const SpriteCatalog* catalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>();

    Entity* entity = new Entity();
    entity->SetDataNameId(Entities::k_PunchAttack);

    RenderableComponent& renderComp = entity->AddComponent<RenderableComponent>();
    if (catalog != nullptr)
    {
        renderComp.SetSprite(catalog->Get(Sprites::k_Punch));
        renderComp.GetSprite().SetZValue(3.0f);
    }

    entity->AddComponent<PhysicableComponent>();

    entity->SetSize(128.0f, 128.0f);

    return Entity::Ptr(entity);
}

//----------------------------------------------------------------------------
Entity::Ptr EnemyCharEntityFactory::Create() const
{
    const SpriteCatalog* catalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>();

    Entity* entity = new Entity();
    entity->SetDataNameId(Entities::k_EnemyChar);

    RenderableComponent& renderComp = entity->AddComponent<RenderableComponent>();
    if (catalog != nullptr)
    {
        renderComp.SetSprite(catalog->Get(Sprites::k_Covid));
        renderComp.GetSprite().SetZValue(2.0f);

        renderComp.GetSprite().PlayAnimation(DataNameIds::Animations::k_IdleL);
    }

    entity->AddComponent<PhysicableComponent>();
    entity->AddComponent<EnemyCharacterComponent>();

    entity->SetSize(50.0f, 50.0f);

    return Entity::Ptr(entity);
}

//----------------------------------------------------------------------------
Entity::Ptr GameRefereeEntityFactory::Create() const
{
    Entity* entity = new Entity();
    entity->SetDataNameId(Entities::k_GameReferee);

    entity->AddComponent<GameRefereeComponent>();

    entity->SetSize(0.0f, 0.0f);

    return Entity::Ptr(entity);
}
