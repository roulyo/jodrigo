#include <data/entities/EntityCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
EntityCatalog::EntityCatalog()
{
    RegisterData(Entities::k_PlayableChar,  m_PlayableCharEntityFactory);
    RegisterData(Entities::k_EnemyChar,     m_EnemyCharEntityFactory);
    RegisterData(Entities::k_PunchAttack,   m_PunchAttackEntityFactory);
    RegisterData(Entities::k_GameReferee,   m_GameRefereeEntityFactory);
}

//----------------------------------------------------------------------------
EntityCatalog::~EntityCatalog()
{
    m_Factories.clear();
}
