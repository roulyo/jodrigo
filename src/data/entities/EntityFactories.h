#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/core/ecs/Entity.h>

#include <data/DataNameIds.h>


//----------------------------------------------------------------------------
class PlayableCharEntityFactory : public AbstractDataFactory
{
public:
    Entity::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class PunchAttackEntityFactory : public AbstractDataFactory
{
public:
    Entity::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class EnemyCharEntityFactory : public AbstractDataFactory
{
public:
    Entity::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class GameRefereeEntityFactory : public AbstractDataFactory
{
public:
    Entity::Ptr Create() const override;

};
