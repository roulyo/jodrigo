#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/core/ecs/Entity.h>

#include <data/entities/EntityFactories.h>

//----------------------------------------------------------------------------
class EntityCatalog : public DataCatalog<Entity>
{
    forge_DeclCatalog(EntityCatalog);

public:
    EntityCatalog();
    ~EntityCatalog();

private:
    PlayableCharEntityFactory   m_PlayableCharEntityFactory;
    EnemyCharEntityFactory      m_EnemyCharEntityFactory;
    PunchAttackEntityFactory    m_PunchAttackEntityFactory;
    GameRefereeEntityFactory    m_GameRefereeEntityFactory;

};
