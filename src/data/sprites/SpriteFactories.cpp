#include <data/sprites/SpriteFactories.h>

#include <data/textures/TextureCatalog.h>
#include <data/animations/AnimationCatalog.h>
#include <data/DataNameIds.h>

#include <forge/system/data/DataManager.h>

#include <string>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Sprite::Ptr JodrigoSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* character;

    if (texCatalog != nullptr)
    {
        character = new Sprite(texCatalog->Get(Textures::k_CharSpriteSheet), sf::IntRect(128 * 0, 256 * 0, 128, 256));
        character->SetDataNameId(Sprites::k_Jodrigo);

        const AnimationCatalog* animationCatalog = DataManager::GetInstance().GetCatalog<AnimationCatalog>();

        if (animationCatalog != nullptr)
        {
            character->AddAnimation(animationCatalog->Get(Animations::k_IdleL));
            character->AddAnimation(animationCatalog->Get(Animations::k_IdleR));
        }
    }

    return Sprite::Ptr(character);
}

//----------------------------------------------------------------------------
Sprite::Ptr CovidSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* character;

    if (texCatalog != nullptr)
    {
        character = new Sprite(texCatalog->Get(Textures::k_CovidSpriteSheet), sf::IntRect(64 * 0, 64 * 0, 64, 64));
        character->SetDataNameId(Sprites::k_Covid);

        const AnimationCatalog* animationCatalog = DataManager::GetInstance().GetCatalog<AnimationCatalog>();

        if (animationCatalog != nullptr)
        {
            character->AddAnimation(animationCatalog->Get(Animations::k_IdleL));
        }
    }

    return Sprite::Ptr(character);
}

//----------------------------------------------------------------------------
Sprite::Ptr PunchSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* character;

    if (texCatalog != nullptr)
    {
        character = new Sprite(texCatalog->Get(Textures::k_PunchSpriteSheet), sf::IntRect(128 * 0, 128 * 0, 128, 128));
        character->SetDataNameId(Sprites::k_Punch);

        const AnimationCatalog* animationCatalog = DataManager::GetInstance().GetCatalog<AnimationCatalog>();

        if (animationCatalog != nullptr)
        {
            character->AddAnimation(animationCatalog->Get(Animations::k_PunchN));
            character->AddAnimation(animationCatalog->Get(Animations::k_PunchS));
            character->AddAnimation(animationCatalog->Get(Animations::k_PunchE));
            character->AddAnimation(animationCatalog->Get(Animations::k_PunchW));
        }
    }

    return Sprite::Ptr(character);
}
