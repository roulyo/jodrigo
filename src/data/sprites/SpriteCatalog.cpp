#include <data/sprites/SpriteCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
SpriteCatalog::SpriteCatalog()
{
    RegisterData(Sprites::k_Jodrigo,    m_JodrigoSpriteFactory);
    RegisterData(Sprites::k_Covid,      m_CovidSpriteFactory);
    RegisterData(Sprites::k_Punch,      m_PunchSpriteFactory);
}

//----------------------------------------------------------------------------
SpriteCatalog::~SpriteCatalog()
{
    m_Factories.clear();
}
