#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/2d/drawable/Sprite.h>

//----------------------------------------------------------------------------
class JodrigoSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class CovidSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;

};

//----------------------------------------------------------------------------
class PunchSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;

};
