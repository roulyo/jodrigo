#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/2d/drawable/Sprite.h>

#include <data/sprites/SpriteFactories.h>

//----------------------------------------------------------------------------
class SpriteCatalog : public DataCatalog<Sprite>
{
    forge_DeclCatalog(SpriteCatalog);

public:
    SpriteCatalog();
    ~SpriteCatalog();

private:
    JodrigoSpriteFactory    m_JodrigoSpriteFactory;
    CovidSpriteFactory      m_CovidSpriteFactory;
    PunchSpriteFactory      m_PunchSpriteFactory;

};
