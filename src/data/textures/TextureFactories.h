#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/2d/drawable/Texture.h>

//----------------------------------------------------------------------------
class CharSpriteSheetFactory : public AbstractDataFactory
{
public:
    Texture::Ptr Create() const override;

private:
    static Texture::Ptr m_Texture;

};

//----------------------------------------------------------------------------
class CovidSpriteSheetFactory : public AbstractDataFactory
{
public:
    Texture::Ptr Create() const override;

private:
    static Texture::Ptr m_Texture;

};

//----------------------------------------------------------------------------
class PunchSpriteSheetFactory : public AbstractDataFactory
{
public:
    Texture::Ptr Create() const override;

private:
    static Texture::Ptr m_Texture;

};
