#include <data/textures/TextureCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
TextureCatalog::TextureCatalog()
{
    RegisterData(Textures::k_CharSpriteSheet,   m_CharSpriteSheetFactory);
    RegisterData(Textures::k_CovidSpriteSheet,  m_CovidSpriteSheetFactory);
    RegisterData(Textures::k_PunchSpriteSheet,  m_PunchSpriteSheetFactory);
}

//----------------------------------------------------------------------------
TextureCatalog::~TextureCatalog()
{
    m_Factories.clear();
}
