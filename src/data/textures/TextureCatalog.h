#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/2d/drawable/Texture.h>

#include <data/textures/TextureFactories.h>

//----------------------------------------------------------------------------
class TextureCatalog : public DataCatalog<Texture>
{
    forge_DeclCatalog(TextureCatalog);

public:
    TextureCatalog();
    ~TextureCatalog();

private:
    CharSpriteSheetFactory  m_CharSpriteSheetFactory;
    CovidSpriteSheetFactory m_CovidSpriteSheetFactory;
    PunchSpriteSheetFactory m_PunchSpriteSheetFactory;

};
