#include <data/textures/TextureFactories.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Texture::Ptr CharSpriteSheetFactory::m_Texture = nullptr;

//----------------------------------------------------------------------------
Texture::Ptr CharSpriteSheetFactory::Create() const
{
    if (m_Texture == nullptr)
    {
        Texture* texture = new Texture("assets/jodrigo_idle.png");
        texture->SetDataNameId(Textures::k_CharSpriteSheet);

        m_Texture.reset(texture);
    }

    return m_Texture;
}

//----------------------------------------------------------------------------
Texture::Ptr CovidSpriteSheetFactory::m_Texture = nullptr;

//----------------------------------------------------------------------------
Texture::Ptr CovidSpriteSheetFactory::Create() const
{
    if (m_Texture == nullptr)
    {
        Texture* texture = new Texture("assets/covid.png");
        texture->SetDataNameId(Textures::k_CovidSpriteSheet);

        m_Texture.reset(texture);
    }

    return m_Texture;
}

//----------------------------------------------------------------------------
Texture::Ptr PunchSpriteSheetFactory::m_Texture = nullptr;

//----------------------------------------------------------------------------
Texture::Ptr PunchSpriteSheetFactory::Create() const
{
    if (m_Texture == nullptr)
    {
        Texture* texture = new Texture("assets/punch.png");
        texture->SetDataNameId(Textures::k_PunchSpriteSheet);

        m_Texture.reset(texture);
    }

    return m_Texture;
}

