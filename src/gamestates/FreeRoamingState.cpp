#include <gamestates/FreeRoamingState.h>

#include <data/DataNameIds.h>
#include <data/audio/MusicCatalog.h>

#include <forge/system/input/InputManager.h>
#include <forge/system/data/DataManager.h>
#include <forge/audio/AudioManager.h>

//----------------------------------------------------------------------------
FreeRoamingState::FreeRoamingState(const AbstractForgeGame& _game)
    : AbstractGameState(_game)
{
    m_Systems.push_back(&m_GameArbitrationSystem);
    m_Systems.push_back(&m_PlayerControlSystem);
    m_Systems.push_back(&m_EnemyControlSystem);
    
    m_Systems.push_back(&m_PhysicsSystem);

    m_Systems.push_back(&m_RenderingSystem);
}

//----------------------------------------------------------------------------
FreeRoamingState::~FreeRoamingState()
{
    m_Systems.clear();
}

//----------------------------------------------------------------------------
void FreeRoamingState::BeginFrame(const long long& _dt)
{
}

//----------------------------------------------------------------------------
void FreeRoamingState::EndFrame(const long long& _dt)
{
}

//----------------------------------------------------------------------------
void FreeRoamingState::OnPush()
{
    AbstractGameState::OnPush();

    InputManager::GetInstance().SetInputMapping(&m_FreeRoamingInputScheme);

    const MusicCatalog* catalog = DataManager::GetInstance().GetCatalog<MusicCatalog>();

    if (catalog != nullptr)
    {
        AudioManager::GetInstance().PlayMusic(catalog->Get(DataNameIds::Musics::k_MainTheme), true);
    }
}

//----------------------------------------------------------------------------
void FreeRoamingState::OnPop()
{
    AbstractGameState::OnPop();

    InputManager::GetInstance().SetInputMapping(nullptr);
}
