#pragma once

#include <forge/system/input/InputMappingScheme.h>
#include <forge/system/input/KeyInputBinder.h>
#include <forge/system/input/MouseInputBinder.h>

#include <events/inputs/MoveEvent.h>
#include <events/inputs/MouseEvent.h>

//----------------------------------------------------------------------------
typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::North>,   OrientedMoveEvent<false, MoveDirection::North>> MoveNBinder;
typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::South>,   OrientedMoveEvent<false, MoveDirection::South>> MoveSBinder;
typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::East>,    OrientedMoveEvent<false, MoveDirection::East>>  MoveEBinder;
typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::West>,    OrientedMoveEvent<false, MoveDirection::West>>  MoveWBinder;

typedef MouseInputBinder<ClickedMouseEvent<true, MouseButton::Left>,    ClickedMouseEvent<false, MouseButton::Left>>    MouseLeft;
typedef MouseInputBinder<ClickedMouseEvent<true, MouseButton::Right>,   ClickedMouseEvent<false, MouseButton::Right>>   MouseRight;

//----------------------------------------------------------------------------
class FreeRoamingInputScheme : public InputMappingScheme
{
public:
    FreeRoamingInputScheme();

private:
    MoveNBinder     m_MoveNBinder;
    MoveSBinder     m_MoveSBinder;
    MoveEBinder     m_MoveEBinder;
    MoveWBinder     m_MoveWBinder;

    MouseLeft       m_MouseLeftBinder;
    MouseRight      m_MouseRightBinder;

};