#include <gamestates/FreeRoamingInputs.h>

#include <forge/system/input/InputTypes.h>

//----------------------------------------------------------------------------
FreeRoamingInputScheme::FreeRoamingInputScheme()
{
    RegisterKeyInput(Keyboard::W, &m_MoveNBinder);
    RegisterKeyInput(Keyboard::Z, &m_MoveNBinder);

    RegisterKeyInput(Keyboard::S, &m_MoveSBinder);

    RegisterKeyInput(Keyboard::D, &m_MoveEBinder);

    RegisterKeyInput(Keyboard::A, &m_MoveWBinder);
    RegisterKeyInput(Keyboard::Q, &m_MoveWBinder);

    RegisterMouseInput(Mouse::Left, &m_MouseLeftBinder);
    RegisterMouseInput(Mouse::Right, &m_MouseRightBinder);
}
