#pragma once

#include <forge/core/ecs/System.h>

#include <components/PlayableCharacterComponent.h>
#include <components/RenderableComponent.h>
#include <components/PhysicableComponent.h>
#include <components/EntityLinkComponent.h>

#include <chrono>

//----------------------------------------------------------------------------
class MoveEvent;
class MouseClickEvent;

//----------------------------------------------------------------------------
class PlayerControlSystem : public System<PlayableCharacterComponent, RenderableComponent, PhysicableComponent, EntityLinkComponent>
{
public:
    PlayerControlSystem();
    ~PlayerControlSystem();

    void Start() override;
    void Stop() override;

    void Execute(const long long& _dt, Entity::Ptr _entity) override;
    
    void OnMoveEvent(const MoveEvent& _event);
    void OnMouseClickEvent(const MouseClickEvent& _event);

private:
    struct AttackRequest
    {
        bool    ShouldAttack = false;
        int     X;
        int     Y;
    };

    enum class AttackType
    {
        None,
        North,
        South,
        East,
        West
    };

private:
    void NormalizeSpeed();
    AttackType GetAttackTypeFromRequest(const Entity::Ptr& _entity) const;

private:
    static constexpr float k_DefaultPlayerSpeed = 3.0f;

    float   m_SpeedX;
    float   m_SpeedY;

    AttackRequest               m_AttackRequest;
    std::chrono::milliseconds   m_AttackStartTimeMS;

};
