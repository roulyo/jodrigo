#include <systems/EnemyControlSystem.h>

#include <forge/core/builtin/EngineEvents.h>

#include <components/PlayableCharacterComponent.h>

//----------------------------------------------------------------------------
void EnemyControlSystem::Start()
{
    EntityAddedEvent::Handlers += EntityAddedEvent::Handler(this, &EnemyControlSystem::OnEntityAddedEvent);
}

//----------------------------------------------------------------------------
void EnemyControlSystem::Stop()
{
    EntityAddedEvent::Handlers -= EntityAddedEvent::Handler(this, &EnemyControlSystem::OnEntityAddedEvent);
}

//----------------------------------------------------------------------------
void EnemyControlSystem::Execute(const long long& _dt, Entity::Ptr _entity)
{
    Entity& entity = *_entity->As<Entity>();
    Entity& target = *m_MainTarget->As<Entity>();

    float targetX = target.GetAABB().left + (target.GetAABB().width / 2.0f);
    float targetY = target.GetAABB().top + (target.GetAABB().height / 2.0f);

    float entityX = entity.GetAABB().left + (entity.GetAABB().width / 2.0f);
    float entityY = entity.GetAABB().top + (entity.GetAABB().height / 2.0f);

    float speedX = targetX - entityX;
    float speedY = targetY - entityY;

    if (speedX != 0.0f || speedY != 0.0f)
    {
        float length = sqrt((speedX * speedX) + (speedY * speedY));

        speedX /= length;
        speedX *= k_DefaultEnemySpeed;

        speedY /= length;
        speedY *= k_DefaultEnemySpeed;

    }

    PhysicableComponent& physicComp = entity.GetComponent<PhysicableComponent>();
    physicComp.SpeedX = speedX;
    physicComp.SpeedY = speedY;
}

//----------------------------------------------------------------------------
void EnemyControlSystem::OnEntityAddedEvent(const EntityAddedEvent& _event)
{
    const Entity::Ptr& entity = _event.GetEntity();

    if (entity->As<Entity>()->HasComponent<PlayableCharacterComponent>())
    {
        m_MainTarget = entity;
    }
}
