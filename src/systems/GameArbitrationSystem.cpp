#include <systems/GameArbitrationSystem.h>

#include <events/physics/CollisionEvent.h>

#include <data/DataNameIds.h>
#include <data/entities/EntityCatalog.h>
#include <data/audio/SoundCatalog.h>
#include <data/fonts/FontCatalog.h>

#include <forge/system/data/DataManager.h>
#include <forge/2d/camera/CameraManager.h>
#include <forge/2d/drawable/Font.h>
#include <forge/audio/AudioManager.h>
#include <forge/system/window/WindowManager.h>

#include <Bahamut.h>
#include <cmath>
#include <cassert>

GameArbitrationSystem::GameArbitrationSystem()
    : m_CollisionCount(0)
    , m_KillCount(0)
    , m_NextSpawnTime()
{
}

//----------------------------------------------------------------------------
void GameArbitrationSystem::Start()
{
    CollisionEvent::Handlers += CollisionEvent::Handler(this, &GameArbitrationSystem::OnCollisionEvent);

    const FontCatalog* fonts = DataManager::GetInstance().GetCatalog<FontCatalog>();

    if (fonts != nullptr)
    {
        const sf::FloatRect& frustum = CameraManager::GetInstance().GetMainCamera().GetFrustum();


        m_ScoreTxt.SetFont(fonts->Get(DataNameIds::Fonts::k_MainFont));
        m_ScoreTxt.SetScreenCoord(32, 32);
        m_ScoreTxt.SetFillColor(40, 40, 40);
        m_ScoreTxt.SetSize(36);

        m_LifeTxt.SetFont(fonts->Get(DataNameIds::Fonts::k_MainFont));
        m_LifeTxt.SetScreenCoord(frustum.width - 200, 32);
        m_LifeTxt.SetFillColor(40, 40, 40);
        m_LifeTxt.SetSize(36);
    }
}

//----------------------------------------------------------------------------
void GameArbitrationSystem::Stop()
{
    CollisionEvent::Handlers -= CollisionEvent::Handler(this, &GameArbitrationSystem::OnCollisionEvent);
}

//----------------------------------------------------------------------------
void GameArbitrationSystem::Execute(const long long& _dt, Entity::Ptr _entity)
{
    GameRefereeComponent& ref = _entity->As<Entity>()->GetComponent<GameRefereeComponent>();


    // Player's Life
    if (m_CollisionCount != 0)
    {
        ref.Life -= m_CollisionCount;

        if (ref.Life < 0)
        {
            // Game Over
            assert(false);
        }

        m_CollisionCount = 0;
    }

    // Kills
    if (m_KillCount != 0)
    {
        ref.Score += m_KillCount * 1000;
        m_KillCount = 0;
    }

    // Spawn
    long long now = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now()).time_since_epoch().count();

    if (now > m_NextSpawnTime.count())
    {
        const EntityCatalog* catalog = DataManager::GetInstance().GetCatalog<EntityCatalog>();
        Entity::Ptr enemy = catalog->Get(DataNameIds::Entities::k_EnemyChar);

        const sf::FloatRect& frustum = CameraManager::GetInstance().GetMainCamera().GetFrustum();
        int randPoint = rand() % static_cast<int>(frustum.width * 2 + frustum.height * 2);
        float x = frustum.left;
        float y = frustum.top;

        if (randPoint < frustum.width)
        {
            x += randPoint;
        }
        else if (randPoint < (frustum.width + frustum.height))
        {
            x += frustum.width - enemy->As<Entity>()->GetAABB().width;
            y += randPoint - frustum.width;
        }
        else if (randPoint < (frustum.width * 2 + frustum.height))
        {
            x += randPoint - frustum.width - frustum.height;
            y += frustum.height - enemy->As<Entity>()->GetAABB().height;
        }
        else
        {
            y += randPoint - frustum.width * 2 - frustum.height;
        }

        enemy->As<Entity>()->SetPosition(x, y);

        g_World->AddEntity(enemy);

        int nextSpawnTime = std::max(500, static_cast<int>(1000.0f / ref.SpawnFrequency));

        m_NextSpawnTime = std::chrono::milliseconds(now + nextSpawnTime);

        ref.SpawnFrequency *= 1.0f + (static_cast<float>(GameRefereeComponent::SpawnIncreaseRatio) / 100);
    }

    ref.Score += _dt / 10000;

    // UI Update
    {
        char buffer[32];

        sprintf_s(buffer, "SCORE: %d", ref.Score);
        m_ScoreTxt.SetString(buffer);
        WindowManager::GetInstance().PushToRender(m_ScoreTxt.GetRenderingData());

        sprintf_s(buffer, "LIFE: %d", ref.Life);
        m_LifeTxt.SetString(buffer);
        WindowManager::GetInstance().PushToRender(m_LifeTxt.GetRenderingData());
    }
}

//----------------------------------------------------------------------------
void GameArbitrationSystem::OnCollisionEvent(const CollisionEvent& _event)
{
    const SoundCatalog* soundCatalog = DataManager::GetInstance().GetCatalog<SoundCatalog>();

    if (    (_event.GetElt1()->GetDataNameId() == DataNameIds::Entities::k_PlayableChar || _event.GetElt2()->GetDataNameId() == DataNameIds::Entities::k_PlayableChar)
        &&  (_event.GetElt1()->GetDataNameId() == DataNameIds::Entities::k_EnemyChar || _event.GetElt2()->GetDataNameId() == DataNameIds::Entities::k_EnemyChar))
    {
        m_CollisionCount++;

        if (_event.GetElt1()->GetDataNameId() == DataNameIds::Entities::k_EnemyChar)
        {
            g_World->RemoveEntity(_event.GetElt1());
        }
        else
        {
            g_World->RemoveEntity(_event.GetElt2());
        }

        if (soundCatalog)
        {
            AudioManager::GetInstance().PlaySound(soundCatalog->Get(DataNameIds::Sounds::k_HitTake));
        }
    }
    else if ((_event.GetElt1()->GetDataNameId() == DataNameIds::Entities::k_PunchAttack || _event.GetElt2()->GetDataNameId() == DataNameIds::Entities::k_PunchAttack)
        &&  (_event.GetElt1()->GetDataNameId() == DataNameIds::Entities::k_EnemyChar || _event.GetElt2()->GetDataNameId() == DataNameIds::Entities::k_EnemyChar))
    {
        m_KillCount++;

        if (_event.GetElt1()->GetDataNameId() == DataNameIds::Entities::k_EnemyChar)
        {
            g_World->RemoveEntity(_event.GetElt1());
        }
        else
        {
            g_World->RemoveEntity(_event.GetElt2());
        }

        if (soundCatalog)
        {
            AudioManager::GetInstance().PlaySound(soundCatalog->Get(DataNameIds::Sounds::k_EnemyDeath));
        }
    }
}
