#pragma once

#include <forge/core/ecs/System.h>

#include <components/PhysicableComponent.h>

//----------------------------------------------------------------------------
template<class T> class Quadtree;

class EntityAddedEvent;
class EntityRemovedEvent;

//----------------------------------------------------------------------------
class PhysicsSystem : public System<PhysicableComponent>
{
public:
    PhysicsSystem();
    ~PhysicsSystem();

    void Start() override;
    void Stop() override;

    void Execute(const long long& _dt, Entity::Ptr _entity) override;

    void OnEntityAddedEvent(const EntityAddedEvent& _event);
    void OnEntityRemovedEvent(const EntityRemovedEvent& _event);

private:
    struct KnownCollision
    {
        Entity::Ptr Elt1;
        Entity::Ptr Elt2;

        bool operator==(const KnownCollision& _rhs)
        {
            return (Elt1 == _rhs.Elt1 && Elt2 == _rhs.Elt2)
                || (Elt1 == _rhs.Elt2 && Elt2 == _rhs.Elt1);
        }
    };

private:
    Quadtree<Data::Ptr>*        m_PhysicWorld;
    
    std::vector<KnownCollision> m_KnownCollisions;

    static constexpr float  k_Gravity = 9.807f;
    static constexpr int    k_UnitPerMeter = 128;

};
