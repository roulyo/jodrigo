#pragma once

#include <forge/core/ecs/System.h>

#include <components/EnemyCharacterComponent.h>
#include <components/PhysicableComponent.h>

//----------------------------------------------------------------------------
class EntityAddedEvent;

//----------------------------------------------------------------------------
class EnemyControlSystem : public System<EnemyCharacterComponent, PhysicableComponent>
{
public:
    void Start() override;
    void Stop() override;

    void Execute(const long long& _dt, Entity::Ptr _entity) override;

    void OnEntityAddedEvent(const EntityAddedEvent& _event);

private:
    static constexpr float k_DefaultEnemySpeed = 1.0f;

    Entity::Ptr m_MainTarget;

};
