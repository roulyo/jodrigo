#pragma once

#include <forge/core/ecs/System.h>

#include <components/GameRefereeComponent.h>

#include <forge/2d/drawable/Text.h>

#include <chrono>

//----------------------------------------------------------------------------
class CollisionEvent;

//----------------------------------------------------------------------------
class GameArbitrationSystem : public System<GameRefereeComponent>
{
public:
    GameArbitrationSystem();

    void Start() override;
    void Stop() override;

    void Execute(const long long& _dt, Entity::Ptr _entity) override;

    void OnCollisionEvent(const CollisionEvent& _event);

private:
    struct KillRequest
    {
        Entity::Ptr Target;
    };

private:
    std::chrono::milliseconds   m_NextSpawnTime;

    int     m_CollisionCount;
    int     m_KillCount;

    Text    m_ScoreTxt;
    Text    m_LifeTxt;

};
