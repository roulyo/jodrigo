#include <systems/RenderingSystem.h>

#include <forge/system/window/WindowManager.h>

#include <forge/2d/camera/CameraManager.h>

//----------------------------------------------------------------------------
void RenderingSystem::Execute(const long long& _dt, Entity::Ptr _entity)
{
    Entity& entity = *_entity->As<Entity>();

    RenderableComponent& renderComp = entity.GetComponent<RenderableComponent>();
    Sprite& sprite = renderComp.GetSprite();
    

    float currentX = entity.GetAABB().left;
    float currentY = entity.GetAABB().top;
    float currentH = entity.GetAABB().height;
    float currentW = entity.GetAABB().width;

    float screenX = 0;
    float screenY = 0;

    CameraManager::GetInstance().GetMainCamera().WorldToScreen(currentX, currentY, screenX, screenY);

    // isometric hack
    //screenX -= static_cast<float>(sprite.GetTextureRect().width) / 2.0f;

    int spriteH = sprite.GetTextureRect().height;
    int spriteW = sprite.GetTextureRect().width;

    screenX -= (spriteW / 2) - static_cast<int>(currentW / 2);
    screenY -= (spriteH / 2) - static_cast<int>(currentH / 2);

    sprite.SetScreenCoord(screenX, screenY);

    RenderingData data = sprite.GetRenderingData();
    data.Coord.x = currentX;
    data.Coord.y = currentY;

    WindowManager::GetInstance().PushToRender(data);
}
