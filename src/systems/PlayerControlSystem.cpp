#include <systems/PlayerControlSystem.h>

#include <forge/system/data/DataManager.h>
#include <forge/2d/camera/CameraManager.h>
#include <forge/audio/AudioManager.h>

#include <events/inputs/MoveEvent.h>
#include <events/inputs/MouseEvent.h>

#include <data/DataNameIds.h>
#include <data/entities/EntityCatalog.h>
#include <data/audio/SoundCatalog.h>

#include <Bahamut.h>

#include <cmath>
#include <chrono>

namespace math
{
    struct Point
    {
        int x, y;
    };

    struct Triangle
    {
        Point a, b, c;
    };

    static bool IsPointInTriangle(const Point& _point, const Triangle& _triangle)
    {
        Point p  = _point;
        Point p0 = _triangle.a;
        Point p1 = _triangle.b;
        Point p2 = _triangle.c;

        float A = 1.f / 2.f * static_cast<float>(-p1.y * p2.x + p0.y * (-p1.x + p2.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y);
        int sign = A < 0 ? -1 : 1;
        float s = static_cast<float>(p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * p.x + (p0.x - p2.x) * p.y) * sign;
        float t = static_cast<float>(p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * p.x + (p1.x - p0.x) * p.y) * sign;

        return s > 0 && t > 0 && (s + t) < 2 * A * sign;
    }
}

//----------------------------------------------------------------------------
PlayerControlSystem::PlayerControlSystem()
    : m_SpeedX(0.0f)
    , m_SpeedY(0.0f)
{ 
}

//----------------------------------------------------------------------------
PlayerControlSystem::~PlayerControlSystem()
{
}

//----------------------------------------------------------------------------
void PlayerControlSystem::Start()
{
    MoveEvent::Handlers += MoveEvent::Handler(this, &PlayerControlSystem::OnMoveEvent);

    MouseClickEvent::Handlers += MouseClickEvent::Handler(this, &PlayerControlSystem::OnMouseClickEvent);
}

//----------------------------------------------------------------------------
void PlayerControlSystem::Stop()
{
    MoveEvent::Handlers -= MoveEvent::Handler(this, &PlayerControlSystem::OnMoveEvent);

    MouseClickEvent::Handlers -= MouseClickEvent::Handler(this, &PlayerControlSystem::OnMouseClickEvent);
}

//----------------------------------------------------------------------------
void PlayerControlSystem::Execute(const long long& _dt, Entity::Ptr _entity)
{
    Entity& entity = *_entity->As<Entity>();

    // Move

    PhysicableComponent& physicComp = entity.GetComponent<PhysicableComponent>();
    physicComp.SpeedX = m_SpeedX;
    physicComp.SpeedY = m_SpeedY;

    RenderableComponent& renderComp = entity.GetComponent<RenderableComponent>();

    if (m_SpeedX > 0) // Going East
    {
        renderComp.GetSprite().PlayAnimation(DataNameIds::Animations::k_IdleR);
    }
    else if (m_SpeedX < 0) // Going West
    {
         renderComp.GetSprite().PlayAnimation(DataNameIds::Animations::k_IdleL);
    }

    {
        EntityLinkComponent& linkComp = entity.GetComponent<EntityLinkComponent>();
        Entity::Ptr punchEntity = linkComp.GetEntityLink();

        if (punchEntity != nullptr)
        {
            PhysicableComponent& punchPhysicComp = punchEntity->As<Entity>()->GetComponent<PhysicableComponent>();
            punchPhysicComp.SpeedX = m_SpeedX;
            punchPhysicComp.SpeedY = m_SpeedY;
        }
    }

    // Attack

    if (m_AttackRequest.ShouldAttack && m_AttackStartTimeMS.count() == 0)
    {
        const EntityCatalog* catalog = DataManager::GetInstance().GetCatalog<EntityCatalog>();

        if (catalog != nullptr)
        {
            Entity::Ptr punch = catalog->Get(DataNameIds::Entities::k_PunchAttack);
            PhysicableComponent& punchPhysicComp = punch->As<Entity>()->GetComponent<PhysicableComponent>();
            punchPhysicComp.SpeedX = m_SpeedX;
            punchPhysicComp.SpeedY = m_SpeedY;

            // position and orientation
            {
                Entity* punchRawPtr = punch->As<Entity>();
                RenderableComponent& punchRenderComp = punchRawPtr->GetComponent<RenderableComponent>();

                AttackType attackType = GetAttackTypeFromRequest(_entity);
                switch (attackType)
                {
                case AttackType::North:
                    punchRawPtr->SetPosition(entity.GetAABB().left, entity.GetAABB().top - punchRawPtr->GetAABB().height);
                    punchRenderComp.GetSprite().PlayAnimation(DataNameIds::Animations::k_PunchN);
                    break;
                case AttackType::South:
                    punchRawPtr->SetPosition(entity.GetAABB().left, entity.GetAABB().top + entity.GetAABB().height);
                    punchRenderComp.GetSprite().PlayAnimation(DataNameIds::Animations::k_PunchS);
                    break;
                case AttackType::East:
                    punchRawPtr->SetPosition(entity.GetAABB().left + entity.GetAABB().width, entity.GetAABB().top);
                    punchRenderComp.GetSprite().PlayAnimation(DataNameIds::Animations::k_PunchE);
                    break;
                case AttackType::West:
                    punchRawPtr->SetPosition(entity.GetAABB().left - punchRawPtr->GetAABB().width, entity.GetAABB().top);
                    punchRenderComp.GetSprite().PlayAnimation(DataNameIds::Animations::k_PunchW);
                    break;
                default:
                    break;
                }
            }

            EntityLinkComponent& linkComp = entity.GetComponent<EntityLinkComponent>();
            linkComp.SetEntityLink(punch);

            if (g_World)
                g_World->AddEntity(punch);

            const SoundCatalog* soundCatalog = DataManager::GetInstance().GetCatalog<SoundCatalog>();

            if (soundCatalog)
                AudioManager::GetInstance().PlaySound(soundCatalog->Get(DataNameIds::Sounds::k_HitGive));

            m_AttackRequest.ShouldAttack = false;
            m_AttackStartTimeMS = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now()).time_since_epoch();
        }
    }
    else if (m_AttackStartTimeMS.count() != 0)
    {
        auto nowMS = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now()).time_since_epoch();
        auto attackDurationMS = nowMS - m_AttackStartTimeMS;

        if (attackDurationMS.count() > 250)
        {
            EntityLinkComponent& linkComp = entity.GetComponent<EntityLinkComponent>();
            Entity::Ptr punchEntity = linkComp.GetEntityLink();

            if (punchEntity != nullptr)
            {
                if (g_World)
                    g_World->RemoveEntity(punchEntity);

                linkComp.SetEntityLink(nullptr);
            }

            m_AttackStartTimeMS = std::chrono::milliseconds();
        }
    }

    m_AttackRequest.ShouldAttack = false;
}

//----------------------------------------------------------------------------
void PlayerControlSystem::OnMoveEvent(const MoveEvent& _event)
{
    MoveDirection direction = _event.GetDirection();

    switch (direction)
    {
    case MoveDirection::North:
        m_SpeedY += _event.IsMoving() ? -k_DefaultPlayerSpeed : -m_SpeedY;
        break;
    case MoveDirection::South:
        m_SpeedY += _event.IsMoving() ? k_DefaultPlayerSpeed : -m_SpeedY;
        break;
    case MoveDirection::East:
        m_SpeedX += _event.IsMoving() ? k_DefaultPlayerSpeed : -m_SpeedX;
        break;
    case MoveDirection::West:
        m_SpeedX += _event.IsMoving() ? -k_DefaultPlayerSpeed : -m_SpeedX;
        break;
    }

    NormalizeSpeed();
}

//----------------------------------------------------------------------------
void PlayerControlSystem::OnMouseClickEvent(const MouseClickEvent& _event)
{
    MouseButton button = _event.GetMouseButton();

    if (_event.GetIsPressed() && button == MouseButton::Left)
    {
        m_AttackRequest.ShouldAttack = true;
        m_AttackRequest.X = _event.GetX();
        m_AttackRequest.Y = _event.GetY();
    }
}

//----------------------------------------------------------------------------
void PlayerControlSystem::NormalizeSpeed()
{
    if (m_SpeedX != 0.0f || m_SpeedY != 0.0f)
    {
        float length = sqrt((m_SpeedX * m_SpeedX) + (m_SpeedY * m_SpeedY));

        m_SpeedX /= length;
        m_SpeedX *= k_DefaultPlayerSpeed;

        m_SpeedY /= length;
        m_SpeedY *= k_DefaultPlayerSpeed;
    }
}

//----------------------------------------------------------------------------
PlayerControlSystem::AttackType PlayerControlSystem::GetAttackTypeFromRequest(const Entity::Ptr& _entity) const
{
    Entity& entity = *_entity->As<Entity>();

    math::Point p = {
        m_AttackRequest.X + CameraManager::GetInstance().GetMainCamera().GetFrustum().left,
        m_AttackRequest.Y + CameraManager::GetInstance().GetMainCamera().GetFrustum().top
    };

    int x = entity.GetAABB().left + (entity.GetAABB().width / 2);
    int y = entity.GetAABB().top + (entity.GetAABB().height / 2);

    // North
    math::Triangle triangle = {
        { x, y },
        { x - 1000, y - 1000 },
        { x + 1000, y - 1000 }
    };

    if (math::IsPointInTriangle(p, triangle))
        return AttackType::North;

    // South
    triangle = {
        { x, y },
        { x + 1000, y + 1000 },
        { x - 1000, y + 1000 }
    };

    if (math::IsPointInTriangle(p, triangle))
        return AttackType::South;

    // East
    triangle = {
        { x, y },
        { x + 1000, y - 1000 },
        { x + 1000, y + 1000 }
    };

    if (math::IsPointInTriangle(p, triangle))
        return AttackType::East;

    // West
    triangle = {
        { x, y },
        { x - 1000, y + 1000 },
        { x - 1000, y - 1000 }
    };

    if (math::IsPointInTriangle(p, triangle))
        return AttackType::West;

    return AttackType::None;
}