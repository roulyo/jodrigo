#include <systems/PhysicsSystem.h>

#include <events/physics/CollisionEvent.h>

#include <forge/core/quadtree/Quadtree.h>
#include <forge/core/builtin/EngineEvents.h>

#include <Bahamut.h>

//----------------------------------------------------------------------------
PhysicsSystem::PhysicsSystem()
{
    m_PhysicWorld = new Quadtree<Data::Ptr>(AABB(0, 0, 2000, 2000));
}

//----------------------------------------------------------------------------
PhysicsSystem::~PhysicsSystem()
{
    delete m_PhysicWorld;
    m_PhysicWorld = nullptr;
}

//----------------------------------------------------------------------------
void PhysicsSystem::Start()
{
    EntityAddedEvent::Handlers += EntityAddedEvent::Handler(this, &PhysicsSystem::OnEntityAddedEvent);
    EntityRemovedEvent::Handlers += EntityRemovedEvent::Handler(this, &PhysicsSystem::OnEntityRemovedEvent);
}

//----------------------------------------------------------------------------
void PhysicsSystem::Stop()
{
    EntityAddedEvent::Handlers -= EntityAddedEvent::Handler(this, &PhysicsSystem::OnEntityAddedEvent);
    EntityRemovedEvent::Handlers -= EntityRemovedEvent::Handler(this, &PhysicsSystem::OnEntityRemovedEvent);
}

//----------------------------------------------------------------------------
void PhysicsSystem::Execute(const long long& _dt, Entity::Ptr _entity)
{
    Entity& entity = *_entity->As<Entity>();

    PhysicableComponent& physicComp = entity.GetComponent<PhysicableComponent>();

    float currentX = entity.GetAABB().left;
    float currentY = entity.GetAABB().top;

    float deltaX = (physicComp.SpeedX * k_UnitPerMeter * _dt) / 1000000.0f;
    float deltaY = (physicComp.SpeedY * k_UnitPerMeter * _dt) / 1000000.0f;

    float newX = currentX + deltaX;
    float newY = currentY + deltaY;

    AABB aabb(static_cast<int>(newX),
              static_cast<int>(newY),
              static_cast<int>(entity.GetAABB().width),
              static_cast<int>(entity.GetAABB().height));
    std::vector<Data::Ptr> result;

    m_PhysicWorld->QueryRange(aabb, result);

    for (Data::Ptr physicElt : result)
    {       
        if (physicElt != _entity)
        {
            KnownCollision col = { physicElt, _entity };
            std::vector<KnownCollision>::iterator it;

            if ((it = std::find(m_KnownCollisions.begin(), m_KnownCollisions.end(), col)) != m_KnownCollisions.end())
            {
                m_KnownCollisions.erase(it);
            }
            else
            {
                m_KnownCollisions.push_back(col);

                AbstractEvent::Ptr event = AbstractEvent::Ptr(new CollisionEvent(physicElt, _entity));
                EventManager::GetInstance().Push(event);
            }
        }
    }

    if (deltaX != 0 || deltaY != 0)
    {
        if (g_World->RemoveEntity(_entity))
        {
            entity.SetPosition(newX, newY);
            g_World->AddEntity(_entity);
        }
    }
}

//----------------------------------------------------------------------------
void PhysicsSystem::OnEntityAddedEvent(const EntityAddedEvent& _event)
{
    const Entity::Ptr& entity = _event.GetEntity();
    
    if (entity->As<Entity>()->HasComponent<PhysicableComponent>())
    {
        sf::FloatRect rect = entity->As<Entity>()->GetAABB();
        AABB aabb(static_cast<int>(rect.left),
                  static_cast<int>(rect.top),
                  static_cast<int>(rect.width),
                  static_cast<int>(rect.height));

        m_PhysicWorld->Insert(entity, aabb);
    }
}

//----------------------------------------------------------------------------
void PhysicsSystem::OnEntityRemovedEvent(const EntityRemovedEvent& _event)
{
    const Entity::Ptr& entity = _event.GetEntity();

    if (entity->As<Entity>()->HasComponent<PhysicableComponent>())
    {
        m_PhysicWorld->Remove(entity, AABB(0, 0, 2000, 2000));
    }
}
