#include <events/physics/CollisionEvent.h>

//----------------------------------------------------------------------------
CollisionEvent::CollisionEvent(const Entity::Ptr& _elt1, const Entity::Ptr& _elt2)
    : m_Elt1(_elt1)
    , m_Elt2(_elt2)
{}

//----------------------------------------------------------------------------
const Entity::Ptr& CollisionEvent::GetElt1() const
{
    return m_Elt1;
}

//----------------------------------------------------------------------------
const Entity::Ptr& CollisionEvent::GetElt2() const
{
    return m_Elt2;
}
