#pragma once

#include <forge/system/event/Event.h>
#include <forge/core/ecs/Entity.h>

//----------------------------------------------------------------------------
class CollisionEvent : public Event<CollisionEvent>
{
    forge_DeclEvent(CollisionEvent);

public:
    CollisionEvent(const Entity::Ptr& _elt1, const Entity::Ptr& _elt2);

    const Entity::Ptr& GetElt1() const;
    const Entity::Ptr& GetElt2() const;

private:
    Entity::Ptr m_Elt1;
    Entity::Ptr m_Elt2;

};
