
//----------------------------------------------------------------------------
template<bool _isMoving, MoveDirection _direction>
OrientedMoveEvent<_isMoving, _direction >::OrientedMoveEvent()
    : MoveEvent(_isMoving, _direction)
{
}
