#pragma once

#include <forge/system/event/Event.h>

//----------------------------------------------------------------------------
enum class MouseButton
{
    Right,
    Left,
    Middle
};

//----------------------------------------------------------------------------
class MouseClickEvent : public Event<MouseClickEvent>
{
    forge_DeclEvent(MouseClickEvent);

public:
    MouseClickEvent(bool _isPressed, unsigned short _x, unsigned short _y, MouseButton _button);

    bool GetIsPressed() const;
    unsigned short GetX() const;
    unsigned short GetY() const;
    MouseButton GetMouseButton() const;

private:
    bool            m_IsPressed;
    unsigned short  m_X;
    unsigned short  m_Y;
    MouseButton     m_MouseButton;

};

//----------------------------------------------------------------------------
template<bool _isPressed, MouseButton _button>
class ClickedMouseEvent : public MouseClickEvent
{
public:
    ClickedMouseEvent(unsigned short _x, unsigned short _y);

};

#include "MouseEvent.inl"
