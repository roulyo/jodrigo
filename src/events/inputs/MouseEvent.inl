
//----------------------------------------------------------------------------
template<bool _isPressed,  MouseButton _button>
ClickedMouseEvent<_isPressed, _button >::ClickedMouseEvent(unsigned short _x, unsigned short _y)
    : MouseClickEvent(_isPressed, _x, _y, _button)
{
}

