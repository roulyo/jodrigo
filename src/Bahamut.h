#include <forge/core/Game.h>

#include <gamestates/FreeRoamingState.h>

//----------------------------------------------------------------------------
extern World*   g_World;

//----------------------------------------------------------------------------
class Bahamut : public AbstractForgeGame
{
public:
    Bahamut();
    ~Bahamut();
    
    void Init(const std::string& _gameName) override;
    void Quit() override;
  
private:
    void InitWorld();

private:
    FreeRoamingState    m_FreeRoamingState;

};
