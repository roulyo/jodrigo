#pragma once

#include <forge/core/ecs/Component.h>

//----------------------------------------------------------------------------
class GameRefereeComponent : public AbstractComponent
{
    forge_DeclComponent(GameRefereeComponent);

public:
    GameRefereeComponent();

    int Score;
    int Life;
    
    float SpawnFrequency;

    static constexpr int SpawnIncreaseRatio = 3;

};