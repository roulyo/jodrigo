#include <components/RenderableComponent.h>

//----------------------------------------------------------------------------
forge_DefComponent(RenderableComponent);

//----------------------------------------------------------------------------
Sprite& RenderableComponent::GetSprite()
{
    return *(static_cast<Sprite*>(m_Sprite.get()));
}

//----------------------------------------------------------------------------
void RenderableComponent::SetSprite(Sprite::Ptr _sprite)
{
    m_Sprite = _sprite;
}
