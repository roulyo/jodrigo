#pragma once

#include <forge/core/ecs/Component.h>

//----------------------------------------------------------------------------
class EnemyCharacterComponent : public AbstractComponent
{
    forge_DeclComponent(EnemyCharacterComponent);
};