#include <components/GameRefereeComponent.h>

//----------------------------------------------------------------------------
forge_DefComponent(GameRefereeComponent);

//----------------------------------------------------------------------------
GameRefereeComponent::GameRefereeComponent()
    : Score(0)
    , Life(3)
    , SpawnFrequency(0.5f)
{
}