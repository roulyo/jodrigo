#pragma once

#include <forge/core/ecs/Entity.h>
#include <forge/core/ecs/Component.h>

//----------------------------------------------------------------------------
class EntityLinkComponent : public AbstractComponent
{
    forge_DeclComponent(EntityLinkComponent);

    Entity::Ptr GetEntityLink();
    void SetEntityLink(Entity::Ptr _entity);

private:
    std::weak_ptr<Data>   m_Entity;

};