#pragma once

#include <forge/core/ecs/Component.h>

#include <forge/2d/drawable/Sprite.h>

//----------------------------------------------------------------------------
class RenderableComponent : public AbstractComponent
{
    forge_DeclComponent(RenderableComponent);

    Sprite& GetSprite();
    void SetSprite(Sprite::Ptr _sprite);

private:
    Sprite::Ptr m_Sprite;

};