#include <components/EntityLinkComponent.h>

//----------------------------------------------------------------------------
forge_DefComponent(EntityLinkComponent);

//----------------------------------------------------------------------------
Entity::Ptr EntityLinkComponent::GetEntityLink()
{
    return m_Entity.lock();
}

//----------------------------------------------------------------------------
void EntityLinkComponent::SetEntityLink(Entity::Ptr _entity)
{
    m_Entity = _entity;
}
