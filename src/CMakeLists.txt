ADD_SUBDIRECTORY( components )
ADD_SUBDIRECTORY( data )
ADD_SUBDIRECTORY( events )
ADD_SUBDIRECTORY( gamestates )
ADD_SUBDIRECTORY( systems )

MACRO_ADD_SOURCES(
	EXE_SRC

	Bahamut.cpp
	Bahamut.h
	main.cpp
	
)