 
# Here should go compiler specific flags and stuff.

# This project uses C++17.
IF(WIN32)
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /Zc:externConstexpr /W3")
ELSE(WIN32)
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -W3")
ENDIF(WIN32)

# Debug has -D_DEBUG
SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -D_DEBUG")

# Release
SET(CMAKE_CXX_FLAGS_RELEASE "-O2")
